# Emisor/Receptor de Radio

Este proyecto es un emisor/receptor de radio genérico que
puede enviar y recibir señales a los aparatos que usen
los transmisores o receptores de radio. Este módulo
puede centralizar la información recolectada por los
nodos remotos o controlar remotamente nodos. Más adelante
se agregará un shield WiFi para poder conectar este nodo
directamente a Internet.

Este arreglo puede funcionar como un rudimentario Gateway
de acceso para Internet de las Cosas utilizando radios
digitales de muy bajo costo.

Prototipo elaborado por: Mario Gómez @ Hackerspace San Salvador

## Lista de Materiales
Para construir este proyecto necesitas los siguientes materiales:

| Nombre                          | Cantidad |
|---------------------------------|----------|
| Arduino UNO R3                  |        1 |
| Transmisor de Radio (RF-TX)     |        1 |
| Receptor de Radio (RF-RX)       |        1 |
| Fuente de 12V                   |        1 |

## Diagrama de Conexión
![](./Docu/Esquematico_bb.png)

## Guía de Conexion en Breadboard
Para este proyecto es recomendable soldar cables al Transmisor/Receptor
de Radio o en su defecto utilizar una breadboard para facilitar el montaje.
En esta guía se especifican únicamente las conexiones directas.

| Componente                         | RA-GND | RA-VCC  | RA-DAT |
|------------------------------------|--------|---------|--------|
| Transmisor de Radio                | AR-GND | AR-VIN  | AR-2   |
| Receptor de Radio                  | AR-GND | AR-5V   | AR-3   |

## Información para Colaborar con este Proyecto
Para colaborar con este proyecto solicita acceso al canal
oficial de Slack en [https://labsresiliencia.slack.com](https://labsresiliencia.slack.com)

