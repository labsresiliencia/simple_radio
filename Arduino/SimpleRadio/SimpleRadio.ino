#include <NeoSWSerial.h>
const int RADIO_RX = 2;
const int RADIO_TX = 3;

NeoSWSerial nss( RADIO_RX, RADIO_TX );

void setup() {
  Serial.begin(9600);
  nss.begin(9600);
}

void loop() {
  // Enviamos datos entre el radio
  // y el puente serie.
  if(nss.available()) {
    Serial.write(nss.read());
  }
  if(Serial.available()) {
    nss.write(Serial.read());
  }
}
